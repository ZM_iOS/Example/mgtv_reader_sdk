//
//  ViewController.m
//  MogoSDKDemo
//
//  Created by jinxiu on 2021/4/21.
//

#import "ViewController.h"
#import <MGTVReaderSDK/MGTVReaderSDK.h>

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UITextView *inputTextView;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapHandler)];
    [self.view addGestureRecognizer:tapGesture];
    
//    self.inputTextView.text = @"kpath://book/bookreader?book_id=4574&chapter_id=130231&book_name=我的";
    self.inputTextView.text = @"kpath://search";
//    self.inputTextView.text = @"kpath://balance";
//    self.inputTextView.text = @"kpath://home/id?tabId=1";
//    self.inputTextView.text = @"kpath://rank/detail?classId=2&menuId=0&orderType=2";
//    self.inputTextView.text = @"kpath://free/todayfree?site=2";
//    self.inputTextView.text = @"kpath://home/id?tabId=1&secondTabId=2";
//    self.inputTextView.text = @"kpath://book/4574";
}

- (IBAction)btnDidClicked:(UIButton *)sender {
    
    NSString *target = self.inputTextView.text;
    
    // 拼接 scheme （可透传 不拼接 scheme = target）
    NSString *scheme = [NSString stringWithFormat:@"zhanbi://deeplink?direct=%@",target];
    
    // fromViewController 打开SDK的vc
    [Col17kManager openReaderFromViewController:self pageWithscheme:scheme];
}

- (void)testScheme {
    /*
     跳转规则：
        书架：kpath://home/id?tabId=0
        书城各频道：kpath://home/id?tabId=1&secondTabId=(\d+) secondTabId的值为tab的索引 独家 0 精选 1 以此类推
        我的：kpath://home/id?tabId=3
        充值：kpath://balance
        书库：kpath://rank/detail?classId=2&menuId=0&orderType=2
        排行：kpath://rank/detail?classId=2&menuId=7&orderType=2
        免费：kpath://free/todayfree?site=2
        专题：kpath://home/id?tabId=2
        书籍详情：kpath://book/(\d+)
        阅读章节：kpath://book/bookreader?book_id=(.+)&chapter_id=(.+)&book_name=(.+)
        搜索：kpath://search
     */
    NSString *target = @"kpath://search";
    
    // 拼接 scheme （可透传 不拼接 scheme = target）
    NSString *scheme = [NSString stringWithFormat:@"zhanbi://deeplink?direct=%@",target];
    
    // fromViewController 打开SDK的vc
    [Col17kManager openReaderFromViewController:self pageWithscheme:scheme];
}

- (void)tapHandler {
    [self.inputTextView resignFirstResponder];
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return 1;
}

@end

//
//  MGTVSDKBridgeProcotol.h
//  MGTV-iPhone
//
//  Created by Meibin Yuan on 2021/4/23.
//  Copyright © 2021 hunantv. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger, MGTVSDKBridgeStatus) {
    MGTVSDKBridgeStatusDefault,
    MGTVSDKBridgeStatusBecomeActive,
    MGTVSDKBridgeStatusEnterBackground,
    MGTVSDKBridgeStatusExit,
};

typedef NS_ENUM(NSInteger, SDKReportType) {
    SDKReportTypePV      = 0,
    SDKReportTypeVV      = 1,
    SDKReportTypeHB      = 2,
    SDKReportTypeShow    = 3,
    SDKReportTypeClick   = 4,
    SDKReportTypeOldPV   = 5,
};

//SDK常用通知
extern NSString * const kMGSDKCancelNotification;

@protocol MGTVSDKBridgeProcotol <NSObject>

@optional

/**
 SDK当前状态
 @param name SDK名称
 @param status 状态
 */
- (void)MGTVSDKBridgeWithName:(NSString *)name status:(MGTVSDKBridgeStatus)status;

/**
 跳转主站页面
 @param name SDK名称
 @param scheme 跳转scheme
 */
- (void)MGTVSDKBridgeWithName:(NSString *)name pageWithscheme:(NSString *)scheme;

/**
 调起分享控件
 @param name SDK名称
 @param shareInfo 分享信息字典，详见文档
 */
- (void)MGTVSDKBridgeWithName:(NSString *)name shareWithInfo:(NSDictionary *)shareInfo;

/**
 获取用户信息
 @param name SDK名称
 @param userInfoArray 所需的用户信息，如nickName等，详见文档
 @return 用户信息字典
 */
- (NSDictionary *)MGTVSDKBridgeWithName:(NSString *)name getUserInfo:(NSArray *)userInfoArray;

/**
 获取设备
 @param name SDK名称
 @param deviceInfoArray 所需的用户信息，如nickName等，详见文档
 @return 用户信息字典
 */
- (NSDictionary *)MGTVSDKBridgeWithName:(NSString *)name getDeviceInfo:(NSArray *)deviceInfoArray;

/**
 调起实名认证弹框
 @param name SDK名称
 @param checkAuthenCallback 实名认证结果回调
 */
- (void)MGTVSDKBridgeWithName:(NSString *)name authentication:(void(^)(BOOL isSuccess))checkAuthenCallback;

#pragma mark -

/**
 数据上报
 @param name SDK名称
 @param dataType 数据上报类型
 @param dataInfo 上报数据
 */
- (void)MGTVSDKBridgeWithName:(NSString *)name postDataWithType:(SDKReportType)dataType dataInfo:(NSDictionary *)dataInfo;


@end

NS_ASSUME_NONNULL_END

//
//  AppDelegate.m
//  MogoSDKDemo
//
//  Created by jinxiu on 2021/4/21.
//

#import "AppDelegate.h"
#import <MGTVReaderSDK/MGTVReaderSDK.h>
#import <YYKit/YYKit.h>
#import "MGTVSDKBridgeProcotol.h"

NSString * const kMGSDKCancelNotification = @"kMGSDKCancelNotification";

@interface AppDelegate ()<MGTVSDKBridgeProcotol>
@property(nonatomic, strong) NSDictionary *getUserInfo;
@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    [self setColSDK];
    return YES;
}

- (void)setColSDK {
    // 初始化SDK
    [Col17kManager initColSDK:self];
    
    // 设置评论
    [Col17kManager setBookCommentCallBack:^(NSDictionary * _Nullable dic) {
        // 评论
        UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:@"温馨提示" message:@"H5评论页面未接入！" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil];
        [alertVC addAction:cancelAction];
        [alertVC addAction:[UIAlertAction actionWithTitle:@"去评论" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        }]];
        [Col17kManager.getTab presentViewController:alertVC animated:YES completion:nil];
    }];
}

- (void)rechargeComplete:(void(^)(void))callback {
    UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:@"温馨提示" message:@"充值页面未接入！" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil];
    [alertVC addAction:cancelAction];
    [alertVC addAction:[UIAlertAction actionWithTitle:@"去充值" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
    }]];
    [Col17kManager.getTab presentViewController:alertVC animated:YES completion:nil];
}

/**
 跳转主站页面
 @param name SDK名称
 @param scheme 跳转scheme
 */
- (void)MGTVSDKBridgeWithName:(NSString *)name pageWithscheme:(NSString *)scheme {
    if ([scheme isEqualToString:@"imgotv://Login"]) {
        UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:@"温馨提示" message:@"登录后才能使用该功能！" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil];
        [alertVC addAction:cancelAction];
        [alertVC addAction:[UIAlertAction actionWithTitle:@"测试环境登录" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
            // 测试数据
            self.getUserInfo = @{@"uuid":@"485b271df8714bf6b554aac7f2b09329",@"nickName":@"mg528779",@"avatar":@"http:\/\/cdnstatic.wenxue.mgtv.com\/test\/user\/avatar\/19\/79\/83\/76508379.jpg-88x88?v=1619421976000",@"gender":@"0",@"mobile":@"",@"token":@"C2B4M80HD96MJEEM93K0"};
            [[NSNotificationCenter defaultCenter] postNotificationName:@"turnOnUpload" object:nil userInfo:nil];
        }]];
        [alertVC addAction:[UIAlertAction actionWithTitle:@"正式环境登录" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
            // 正式环境
            self.getUserInfo = @{@"uuid":@"b69dbf3e2b8440f3af757fc1245be033",@"nickName":@"mg528779",@"avatar":@"http:\/\/cdnstatic.wenxue.mgtv.com\/test\/user\/avatar\/19\/79\/83\/76508379.jpg-88x88?v=1619421976000",@"gender":@"0",@"mobile":@"",@"token":@"C341DF7E20NGBS6F8EVG"};
            [[NSNotificationCenter defaultCenter] postNotificationName:@"turnOnUpload" object:nil userInfo:nil];
        }]];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [Col17kManager.getTab presentViewController:alertVC animated:YES completion:nil];
        });
    }
    else if ([scheme containsString:@"imgotv://webview"]) {
        UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:@"温馨提示" message:@"跳转到webview 页面" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil];
        [alertVC addAction:cancelAction];
        [Col17kManager.getTab presentViewController:alertVC animated:YES completion:nil];
    }
}

/**
 调起分享控件
 @param name SDK名称
 @param shareInfo 分享信息字典，详见文档
 */
- (void)MGTVSDKBridgeWithName:(NSString *)name shareWithInfo:(NSDictionary *)shareInfo {
    NSLog(@"分享回调：%@",shareInfo);
    NSString *shareString = [shareInfo jsonStringEncoded];
    UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:@"分享信息" message:shareString preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil];
    [alertVC addAction:cancelAction];
    [alertVC addAction:[UIAlertAction actionWithTitle:@"去分享" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
    }]];
    [Col17kManager.getTab presentViewController:alertVC animated:YES completion:nil];
}

/**
 获取用户信息
 @param name SDK名称
 @param userInfoArray 所需的用户信息，如nickName等，详见文档
 @return 用户信息字典
 */
- (NSDictionary *)MGTVSDKBridgeWithName:(NSString *)name getUserInfo:(NSArray *)userInfoArray {
    return self.getUserInfo;
}

@end

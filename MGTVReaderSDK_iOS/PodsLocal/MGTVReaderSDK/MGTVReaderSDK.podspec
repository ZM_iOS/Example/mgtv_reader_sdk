#
#  Be sure to run `pod spec lint ZNReader.podspec' to ensure this is a
#  valid spec and to remove all comments including this before submitting the spec.
#
#  To learn more about Podspec attributes see http://docs.cocoapods.org/specification.html
#  To see working Podspecs in the CocoaPods repo see https://github.com/CocoaPods/Specs/
#

Pod::Spec.new do |s|

  s.name         = "MGTVReaderSDK"
  s.version      = "1.0.0"
  s.license      = { :type => "MIT", :file => "LICENSE" }
  s.summary      = "阅读SDK"
  
  s.homepage     = "https://gitlab.com/ZM_iOS/project/MGTVReaderSDK"
  s.source       = { :git => "https://gitlab.com/ZM_iOS/project/MGTVReaderSDK.git", :tag => "#{s.version}" }
  s.author       = { "Guess" => "zengxsh@col.com" }
  
  s.vendored_frameworks = 'MGTVReaderSDK/*.xcframework'
  s.resource = 'MGTVReaderSDK/MGReaderSDKSource.bundle'
  s.requires_arc = true
  s.platform     = :ios, "9.0"
  s.frameworks   = "UIKit", "CoreText"
  
  s.dependency 'AFNetworking', '4.0.1'
  s.dependency 'Masonry', '1.1.0'
  s.dependency 'MJRefresh'
  s.dependency 'KVOController','1.1.0'
      
  #UITableViewCell 高度优化工具
  s.dependency 'UITableView+FDTemplateLayoutCell' , '~> 1.0'
  
  #数据库集成.
  s.dependency 'WHC_ModelSqliteKit' , '~> 1.0'
  
  #钥匙串存储
  s.dependency 'SAMKeychain' , '~> 1.0'
  
  # 导航
  s.dependency 'HBDNavigationBar' , '~> 1.0'

  # 正则
  s.dependency 'RegexKitLite-NoWarning' , '~> 1.0'

  # 轮播
  #s.dependency 'SDCycleScrollView','>= 1.82'
  
end

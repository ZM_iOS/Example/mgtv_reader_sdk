//
//  Col17kManager.h
//  novelReader
//
//  Created by jinxiu on 2021/4/1.
//  Copyright © 2021 ChineseAll. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@protocol MGTVSDKBridgeProcotol;

NS_ASSUME_NONNULL_BEGIN

/// SDK书 去评论
typedef void(^Col17kBookCommentCallBack)(NSDictionary *_Nullable);

@interface Col17kManager : NSObject

@property(nullable,nonatomic,weak) id<MGTVSDKBridgeProcotol> aDdelegate;                       // default nil. weak reference


+ (instancetype)shareManager;

/// version
- (NSString *)version;

/// buildVersion
- (NSString *)buildVersion;

/// infoDictionary
- (NSDictionary *)infoDictionary;

/// 初始化SDK
+ (void)initColSDK:(id<MGTVSDKBridgeProcotol>)appDelegate;

/// 设置SDK内 书籍评论回调
+ (void)setBookCommentCallBack:(Col17kBookCommentCallBack)callback;

/// Tabbar
+ (UIViewController *)getTab;

/// 关闭小说阅读模块
///
/// - Parameters:
///   - animated:  是否动画执行， SDK.tabbarVC.dismissViewControllerAnimated:animated
///   - completion:  关闭后执行事件
+(void)closeReaderHomeAnimated:(BOOL)animated completion:(void (^ __nullable)(void))completion ;

/// 打开阅读SDK
///
/// - Parameters:
///   - viewController:  打开SDK的VC， SDK.tabbarVC.presentingViewController
///   - scheme:  跳转scheme 可为空
+ (void)openReaderFromViewController:(UIViewController * _Nullable)viewController pageWithscheme:( NSString * _Nullable)scheme;

@end

NS_ASSUME_NONNULL_END

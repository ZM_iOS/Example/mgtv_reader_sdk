//
//  SDImageCache+cycle.h
//  MGTVReaderSDK
//
//  Created by jinxiu on 2021/4/28.
//

#import "SDImageCache.h"

NS_ASSUME_NONNULL_BEGIN

@interface SDImageCache (cycle)
- (void)clearWithCacheType:(SDImageCacheType)cacheType
                completion:(nullable SDWebImageNoParamsBlock)completionBlock;
@end

NS_ASSUME_NONNULL_END

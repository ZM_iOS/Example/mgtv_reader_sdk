//
//  MGGrayPolicyProtocol.h
//  MGTV-iPhone
//
//  Created by xiangbo on 2021/1/26.
//  Copyright © 2021 hunantv. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol MGGrayPolicyProtocol <NSObject>

@optional

- (UIImage *)grayPolicyImage:(UIImage *)originImage key:(NSString *)key;

@end

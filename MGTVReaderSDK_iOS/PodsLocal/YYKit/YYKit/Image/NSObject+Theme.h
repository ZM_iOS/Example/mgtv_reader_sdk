//
//  NSObject+Theme.h
//  MGTV-iPhone
//
//  Created by yuanmeibin on 2017/10/23.
//  Copyright © 2017年 hunantv. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (Theme)

@property (nonatomic, copy)  NSMutableDictionary *themeMap;

/**
 某些控件需要跟随 Channel Auto Play 模版定制，而且优先级高于主题。
 设为YES，不再跟随主题变化；设为NO，会恢复到主题设置且跟随主题变化。
 */
//@property (nonatomic, assign)  BOOL isCAPEnabled;

/**
某些控件需要定制，而且优先级高于主题。
设为YES，不再跟随主题变化；设为NO，会恢复到主题设置且跟随主题变化。
*/
@property (nonatomic, assign) BOOL styleEnabled;

/**
控件归属的页面id
用于控制控件是否支持灰度策略。
*/
@property (nonatomic, strong) NSString *pageIdentify;

/**
控件需要定制，而且优先级高于主题
设为YES，会接收制灰策略。
*/
@property (nonatomic, assign) BOOL grayScaleEnabled;

- (void)changeTheme;

- (void)grayPolicyChange;

- (UIImage *)mgImageWithName:(NSString *)imageName;

//本地灰度策略对应的图片 主要针对于不随主题变化的本地图片置灰
- (UIImage *)mgGPImageWithName:(NSString *)imageName;

//灰度策略下的颜色值
- (UIColor *)mgColorWithName:(NSString *)colorName;

//灰度策略下的颜色值 主要针对于不随主题变化颜色设置
- (UIColor *)mgGPColorWithColor:(UIColor *)color;

//灰度策略下的颜色值 并存储到map里面
- (UIColor *)mgGPColorWithColor:(UIColor *)color key:(NSString *)key;

- (NSURL *)mgGImageUrlWithAddr:(NSString *)url;

- (NSURL *)mgGImageUrlWithURL:(NSURL *)imageURL;

- (NSAttributedString *)mgGAttributes:(NSAttributedString *)attrs;

@end
